package com.pms.sdk.push.mqtt;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.Log;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;

import java.net.URI;

/**
 * Private PUSH Restart Receiver
 * @author haewon
 *
 */
public class RestartReceiver extends BroadcastReceiver implements IPMSConsts
{

	@Override
	public synchronized void onReceive (final Context context, Intent intent)
	{
		/**
		 * 2020.10.15 손보광
		 * 업체 요청으로 Private 소스코드 제거(Private 사용하지 않음)
		 */
	}
}