package com.pms.sdk.push.mqtt;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.common.util.CLog;

public class MQTTService extends Service implements IPMSConsts
{
	/**
	 * 2020.10.15 손보광
	 * 업체 요청으로 Private 소스코드 제거(Private 사용하지 않음)
	 */
	@Override
	public IBinder onBind(Intent intent) { return null; }

	@Override
	public void onCreate()
	{
		CLog.d("onCreate()");
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		CLog.d("onStartCommand()");

		return START_NOT_STICKY;
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
	}
}
